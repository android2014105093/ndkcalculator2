package example.ndkcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("calculate");
    }
    public native double add(double x, double y);
    public native double mul(double x, double y);
    public native double div(double x, double y);
    public native double sub(double x, double y);

    public TextView result;
    public TextView exp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = (TextView) findViewById(R.id.result_text);
        exp = (TextView) findViewById(R.id.exp_text);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_eql:
                if (exp.getText() == "")
                    break;

                String[] ary = parseExp(exp.getText().toString());

                if (ary.length == 1) {
                    result.setText(exp.getText());
                    exp.setText("");
                    break;
                }
                else if (ary.length % 2 != 1) {
                    result.setText("Invalid expression");
                    exp.setText("");
                    break;
                }

                exp.setText("");
                double res = calculate(Double.parseDouble(ary[0]), Double.parseDouble(ary[2]), ary[1]);
                double num;
                String op = null;
                for (int i = 3; i < ary.length; i++) {
                    String element = ary[i];
                    if (i % 2 == 1)
                          op = ary[i];
                    else {
                        num = Double.parseDouble(ary[i]);
                        calculate(res, num, op);
                    }
                }
                result.setText(res + "");
                break;
            case R.id.btn_add:
                exp.setText(exp.getText() + "+");
                break;
            case R.id.btn_sub:
                exp.setText(exp.getText() + "-");
                break;
            case R.id.btn_mul:
                exp.setText(exp.getText() + "*");
                break;
            case R.id.btn_div:
                exp.setText(exp.getText() + "/");
                break;
            case R.id.btn_dot:
                exp.setText(exp.getText() + ".");
                break;
            case R.id.btn_0:
                exp.setText(exp.getText() + "0");
                break;
            case R.id.btn_1:
                exp.setText(exp.getText() + "1");
                break;
            case R.id.btn_2:
                exp.setText(exp.getText() + "2");
                break;
            case R.id.btn_3:
                exp.setText(exp.getText() + "3");
                break;
            case R.id.btn_4:
                exp.setText(exp.getText() + "4");
                break;
            case R.id.btn_5:
                exp.setText(exp.getText() + "5");
                break;
            case R.id.btn_6:
                exp.setText(exp.getText() + "6");
                break;
            case R.id.btn_7:
                exp.setText(exp.getText() + "7");
                break;
            case R.id.btn_8:
                exp.setText(exp.getText() + "8");
                break;
            case R.id.btn_9:
                exp.setText(exp.getText() + "9");
                break;
        }
    }

    private String[] parseExp(String exp) {
        return exp.split("(?<=[-+*/])|(?=[-+*/])");
    }

    private double calculate(double n1, double n2, String op) {
        switch(op.charAt(0)) {
            case '+':
                return add(n1, n2);
            case '-':
                return sub(n1, n2);
            case '*':
                return mul(n1, n2);
            case '/':
                return div(n1, n2);
        }
        return 0;
    }
}
