#include <jni.h>
#ifndef _Included_ex_ndkcalculator_Main
#define _Included_ex_ndkcalculator_Main
#ifdef __cplusplus
extern "C" {
#endif
jint Java_example_ndkcalculator_MainActivity_add(JNIEnv *env, jobject obj, jdouble value1, jdouble value2 ){
    return (jdouble)(value1 + value2);
}
jint Java_example_ndkcalculator_MainActivity_mul(JNIEnv *env, jobject obj, jdouble value1, jdouble value2 ){
    return (jdouble)(value1 * value2);
}
jint Java_example_ndkcalculator_MainActivity_div(JNIEnv *env, jobject obj, jdouble value1, jdouble value2 ){
    return (jdouble)(value1 / value2);
}
jint Java_example_ndkcalculator_MainActivity_sub(JNIEnv *env, jobject obj, jdouble value1, jdouble value2 ){
    return (jdouble)(value1 - value2);
}
#ifdef __cplusplus
}
#endif
#endif